﻿namespace Sample_DCS_CS01
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxSocketConnection = new System.Windows.Forms.GroupBox();
            this.textBoxXSDU = new System.Windows.Forms.TextBox();
            this.textBoxACN = new System.Windows.Forms.TextBox();
            this.textBoxCN = new System.Windows.Forms.TextBox();
            this.labelACN = new System.Windows.Forms.Label();
            this.labelCN = new System.Windows.Forms.Label();
            this.textBoxUN = new System.Windows.Forms.TextBox();
            this.textBoxOPR = new System.Windows.Forms.TextBox();
            this.textBoxPLT = new System.Windows.Forms.TextBox();
            this.labelXSDU = new System.Windows.Forms.Label();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.labelUN = new System.Windows.Forms.Label();
            this.textBoxHost = new System.Windows.Forms.TextBox();
            this.labelOPR = new System.Windows.Forms.Label();
            this.labePLT = new System.Windows.Forms.Label();
            this.labelPort = new System.Windows.Forms.Label();
            this.labelHost = new System.Windows.Forms.Label();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.buttonXML = new System.Windows.Forms.Button();
            this.groupBoxGoals = new System.Windows.Forms.GroupBox();
            this.richTextBoxGoals = new System.Windows.Forms.RichTextBox();
            this.richTextBoxXML = new System.Windows.Forms.RichTextBox();
            this.tabControlLessons = new System.Windows.Forms.TabControl();
            this.tabPageLesson1 = new System.Windows.Forms.TabPage();
            this.labelXML = new System.Windows.Forms.Label();
            this.listViewEvents = new System.Windows.Forms.ListView();
            this.columnTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnEvent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBoxSocketConnection.SuspendLayout();
            this.groupBoxGoals.SuspendLayout();
            this.tabControlLessons.SuspendLayout();
            this.tabPageLesson1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxSocketConnection
            // 
            this.groupBoxSocketConnection.Controls.Add(this.textBoxXSDU);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxACN);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxCN);
            this.groupBoxSocketConnection.Controls.Add(this.labelACN);
            this.groupBoxSocketConnection.Controls.Add(this.labelCN);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxUN);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxOPR);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxPLT);
            this.groupBoxSocketConnection.Controls.Add(this.labelXSDU);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxPort);
            this.groupBoxSocketConnection.Controls.Add(this.labelUN);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxHost);
            this.groupBoxSocketConnection.Controls.Add(this.labelOPR);
            this.groupBoxSocketConnection.Controls.Add(this.labePLT);
            this.groupBoxSocketConnection.Controls.Add(this.labelPort);
            this.groupBoxSocketConnection.Controls.Add(this.labelHost);
            this.groupBoxSocketConnection.Location = new System.Drawing.Point(12, 12);
            this.groupBoxSocketConnection.Name = "groupBoxSocketConnection";
            this.groupBoxSocketConnection.Size = new System.Drawing.Size(931, 105);
            this.groupBoxSocketConnection.TabIndex = 0;
            this.groupBoxSocketConnection.TabStop = false;
            this.groupBoxSocketConnection.Text = "Platform Connection";
            // 
            // textBoxXSDU
            // 
            this.textBoxXSDU.Location = new System.Drawing.Point(302, 79);
            this.textBoxXSDU.Name = "textBoxXSDU";
            this.textBoxXSDU.Size = new System.Drawing.Size(312, 20);
            this.textBoxXSDU.TabIndex = 13;
            // 
            // textBoxACN
            // 
            this.textBoxACN.Location = new System.Drawing.Point(804, 47);
            this.textBoxACN.Name = "textBoxACN";
            this.textBoxACN.Size = new System.Drawing.Size(121, 20);
            this.textBoxACN.TabIndex = 12;
            // 
            // textBoxCN
            // 
            this.textBoxCN.Location = new System.Drawing.Point(804, 17);
            this.textBoxCN.Name = "textBoxCN";
            this.textBoxCN.Size = new System.Drawing.Size(121, 20);
            this.textBoxCN.TabIndex = 11;
            // 
            // labelACN
            // 
            this.labelACN.AutoSize = true;
            this.labelACN.Location = new System.Drawing.Point(651, 50);
            this.labelACN.Name = "labelACN";
            this.labelACN.Size = new System.Drawing.Size(147, 13);
            this.labelACN.TabIndex = 10;
            this.labelACN.Text = "Alternate Name (CUPPSACN)";
            // 
            // labelCN
            // 
            this.labelCN.AutoSize = true;
            this.labelCN.Location = new System.Drawing.Point(651, 20);
            this.labelCN.Name = "labelCN";
            this.labelCN.Size = new System.Drawing.Size(143, 13);
            this.labelCN.TabIndex = 9;
            this.labelCN.Text = "Computer Name (CUPPSCN)";
            // 
            // textBoxUN
            // 
            this.textBoxUN.Location = new System.Drawing.Point(534, 47);
            this.textBoxUN.Name = "textBoxUN";
            this.textBoxUN.Size = new System.Drawing.Size(80, 20);
            this.textBoxUN.TabIndex = 8;
            // 
            // textBoxOPR
            // 
            this.textBoxOPR.Location = new System.Drawing.Point(534, 17);
            this.textBoxOPR.Name = "textBoxOPR";
            this.textBoxOPR.Size = new System.Drawing.Size(80, 20);
            this.textBoxOPR.TabIndex = 7;
            // 
            // textBoxPLT
            // 
            this.textBoxPLT.Location = new System.Drawing.Point(115, 17);
            this.textBoxPLT.Name = "textBoxPLT";
            this.textBoxPLT.Size = new System.Drawing.Size(80, 20);
            this.textBoxPLT.TabIndex = 6;
            // 
            // labelXSDU
            // 
            this.labelXSDU.AutoSize = true;
            this.labelXSDU.Location = new System.Drawing.Point(207, 82);
            this.labelXSDU.Name = "labelXSDU";
            this.labelXSDU.Size = new System.Drawing.Size(71, 13);
            this.labelXSDU.TabIndex = 5;
            this.labelXSDU.Text = "XSD Schema";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(302, 47);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(80, 20);
            this.textBoxPort.TabIndex = 4;
            // 
            // labelUN
            // 
            this.labelUN.AutoSize = true;
            this.labelUN.Location = new System.Drawing.Point(410, 50);
            this.labelUN.Name = "labelUN";
            this.labelUN.Size = new System.Drawing.Size(118, 13);
            this.labelUN.TabIndex = 2;
            this.labelUN.Text = "UserName (CUPPSUN)";
            // 
            // textBoxHost
            // 
            this.textBoxHost.Location = new System.Drawing.Point(302, 17);
            this.textBoxHost.Name = "textBoxHost";
            this.textBoxHost.Size = new System.Drawing.Size(80, 20);
            this.textBoxHost.TabIndex = 3;
            // 
            // labelOPR
            // 
            this.labelOPR.AutoSize = true;
            this.labelOPR.Location = new System.Drawing.Point(410, 20);
            this.labelOPR.Name = "labelOPR";
            this.labelOPR.Size = new System.Drawing.Size(116, 13);
            this.labelOPR.TabIndex = 1;
            this.labelOPR.Text = "Operator (CUPPSOPR)";
            // 
            // labePLT
            // 
            this.labePLT.AutoSize = true;
            this.labePLT.Location = new System.Drawing.Point(10, 20);
            this.labePLT.Name = "labePLT";
            this.labePLT.Size = new System.Drawing.Size(99, 13);
            this.labePLT.TabIndex = 0;
            this.labePLT.Text = "Site (CUPPSPLT) : ";
            // 
            // labelPort
            // 
            this.labelPort.AutoSize = true;
            this.labelPort.Location = new System.Drawing.Point(203, 50);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(85, 13);
            this.labelPort.TabIndex = 1;
            this.labelPort.Text = "Port (CUPPSPP)";
            // 
            // labelHost
            // 
            this.labelHost.AutoSize = true;
            this.labelHost.Location = new System.Drawing.Point(203, 20);
            this.labelHost.Name = "labelHost";
            this.labelHost.Size = new System.Drawing.Size(93, 13);
            this.labelHost.TabIndex = 0;
            this.labelHost.Text = "Node (CUPPSPN)";
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(452, 38);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(192, 23);
            this.buttonConnect.TabIndex = 2;
            this.buttonConnect.Text = "1 - Network Socket Connection";
            this.buttonConnect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxLog.Location = new System.Drawing.Point(6, 140);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBoxLog.Size = new System.Drawing.Size(638, 164);
            this.richTextBoxLog.TabIndex = 7;
            this.richTextBoxLog.Text = "";
            this.richTextBoxLog.DoubleClick += new System.EventHandler(this.richTextBoxLog_DoubleClick);
            // 
            // buttonXML
            // 
            this.buttonXML.Location = new System.Drawing.Point(452, 67);
            this.buttonXML.Name = "buttonXML";
            this.buttonXML.Size = new System.Drawing.Size(192, 23);
            this.buttonXML.TabIndex = 9;
            this.buttonXML.Text = "2 - InterfacesLevelAvailableRequest";
            this.buttonXML.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonXML.UseVisualStyleBackColor = true;
            this.buttonXML.Click += new System.EventHandler(this.buttonXML_Click);
            // 
            // groupBoxGoals
            // 
            this.groupBoxGoals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxGoals.Controls.Add(this.richTextBoxGoals);
            this.groupBoxGoals.Location = new System.Drawing.Point(676, 137);
            this.groupBoxGoals.Name = "groupBoxGoals";
            this.groupBoxGoals.Size = new System.Drawing.Size(267, 462);
            this.groupBoxGoals.TabIndex = 11;
            this.groupBoxGoals.TabStop = false;
            this.groupBoxGoals.Text = "Lesson Goals";
            // 
            // richTextBoxGoals
            // 
            this.richTextBoxGoals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxGoals.BackColor = System.Drawing.Color.Khaki;
            this.richTextBoxGoals.Location = new System.Drawing.Point(8, 19);
            this.richTextBoxGoals.Name = "richTextBoxGoals";
            this.richTextBoxGoals.Size = new System.Drawing.Size(255, 437);
            this.richTextBoxGoals.TabIndex = 0;
            this.richTextBoxGoals.Text = "";
            // 
            // richTextBoxXML
            // 
            this.richTextBoxXML.Location = new System.Drawing.Point(6, 38);
            this.richTextBoxXML.Name = "richTextBoxXML";
            this.richTextBoxXML.Size = new System.Drawing.Size(440, 96);
            this.richTextBoxXML.TabIndex = 12;
            this.richTextBoxXML.Text = "";
            // 
            // tabControlLessons
            // 
            this.tabControlLessons.Controls.Add(this.tabPageLesson1);
            this.tabControlLessons.Location = new System.Drawing.Point(12, 123);
            this.tabControlLessons.Name = "tabControlLessons";
            this.tabControlLessons.SelectedIndex = 0;
            this.tabControlLessons.Size = new System.Drawing.Size(658, 336);
            this.tabControlLessons.TabIndex = 13;
            // 
            // tabPageLesson1
            // 
            this.tabPageLesson1.Controls.Add(this.labelXML);
            this.tabPageLesson1.Controls.Add(this.richTextBoxXML);
            this.tabPageLesson1.Controls.Add(this.buttonXML);
            this.tabPageLesson1.Controls.Add(this.buttonConnect);
            this.tabPageLesson1.Controls.Add(this.richTextBoxLog);
            this.tabPageLesson1.Location = new System.Drawing.Point(4, 22);
            this.tabPageLesson1.Name = "tabPageLesson1";
            this.tabPageLesson1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLesson1.Size = new System.Drawing.Size(650, 310);
            this.tabPageLesson1.TabIndex = 0;
            this.tabPageLesson1.Text = "Lesson 1";
            this.tabPageLesson1.UseVisualStyleBackColor = true;
            // 
            // labelXML
            // 
            this.labelXML.AutoSize = true;
            this.labelXML.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelXML.Location = new System.Drawing.Point(126, 22);
            this.labelXML.Name = "labelXML";
            this.labelXML.Size = new System.Drawing.Size(239, 13);
            this.labelXML.TabIndex = 13;
            this.labelXML.Text = "XML Message : interfaceLevelsAvailableRequest";
            // 
            // listViewEvents
            // 
            this.listViewEvents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnTime,
            this.columnEvent});
            this.listViewEvents.FullRowSelect = true;
            this.listViewEvents.GridLines = true;
            this.listViewEvents.Location = new System.Drawing.Point(12, 461);
            this.listViewEvents.Name = "listViewEvents";
            this.listViewEvents.Size = new System.Drawing.Size(658, 138);
            this.listViewEvents.TabIndex = 14;
            this.listViewEvents.UseCompatibleStateImageBehavior = false;
            this.listViewEvents.View = System.Windows.Forms.View.Details;
            // 
            // columnTime
            // 
            this.columnTime.Tag = "";
            this.columnTime.Text = "Time";
            this.columnTime.Width = 200;
            // 
            // columnEvent
            // 
            this.columnEvent.Text = "Event";
            this.columnEvent.Width = 400;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 611);
            this.Controls.Add(this.listViewEvents);
            this.Controls.Add(this.tabControlLessons);
            this.Controls.Add(this.groupBoxGoals);
            this.Controls.Add(this.groupBoxSocketConnection);
            this.Name = "Form1";
            this.Text = "CUPPT SDK - lesson 01 C#";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBoxSocketConnection.ResumeLayout(false);
            this.groupBoxSocketConnection.PerformLayout();
            this.groupBoxGoals.ResumeLayout(false);
            this.tabControlLessons.ResumeLayout(false);
            this.tabPageLesson1.ResumeLayout(false);
            this.tabPageLesson1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxSocketConnection;
        private System.Windows.Forms.TextBox textBoxHost;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.Label labelHost;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.Label labePLT;
        private System.Windows.Forms.Label labelUN;
        private System.Windows.Forms.Label labelOPR;
        private System.Windows.Forms.Button buttonXML;
        private System.Windows.Forms.GroupBox groupBoxGoals;
        private System.Windows.Forms.RichTextBox richTextBoxGoals;
        private System.Windows.Forms.RichTextBox richTextBoxXML;
        private System.Windows.Forms.TabControl tabControlLessons;
        private System.Windows.Forms.TabPage tabPageLesson1;
        private System.Windows.Forms.ListView listViewEvents;
        private System.Windows.Forms.ColumnHeader columnTime;
        private System.Windows.Forms.ColumnHeader columnEvent;
        private System.Windows.Forms.TextBox textBoxPLT;
        private System.Windows.Forms.Label labelXSDU;
        private System.Windows.Forms.TextBox textBoxUN;
        private System.Windows.Forms.TextBox textBoxOPR;
        private System.Windows.Forms.TextBox textBoxXSDU;
        private System.Windows.Forms.TextBox textBoxACN;
        private System.Windows.Forms.TextBox textBoxCN;
        private System.Windows.Forms.Label labelACN;
        private System.Windows.Forms.Label labelCN;
        private System.Windows.Forms.Label labelXML;
    }
}

