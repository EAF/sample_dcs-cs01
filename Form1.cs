﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using Microsoft.Win32;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml;
using System.IO;
using System.Collections;

namespace Sample_DCS_CS01
{

    //create delegate that allows other threads to send the Form
    delegate void richTextBoxLogAppend(char T, string str);

    public partial class Form1 : Form
    {
        //
        Socket clientSocket;

        //reading thread
        Thread DataReceived;

        //XML Message ID
        int messageID;

        //last XSD version
        string XSDVersion = "01.00.0003";

        //constructor
        public Form1()
        {
            InitializeComponent();
        }

        //load application variables
        private void Form1_Load(object sender, EventArgs e)
        {
            //resize ListViewEvents Columns
            listViewEvents.AutoResizeColumns(System.Windows.Forms.ColumnHeaderAutoResizeStyle.HeaderSize);

            //load Environment Variables
            loadEnvironmentVariables();

            //load text goals
            loadGoals();
        }

        //load Environment Variables
        private void loadEnvironmentVariables()
        {
            textBoxHost.Text = System.Environment.GetEnvironmentVariable("CUPPSPN", EnvironmentVariableTarget.User);
            textBoxPort.Text = System.Environment.GetEnvironmentVariable("CUPPSPP", EnvironmentVariableTarget.User);

            textBoxPLT.Text = System.Environment.GetEnvironmentVariable("CUPPSPLT", EnvironmentVariableTarget.User);
            textBoxCN.Text = System.Environment.GetEnvironmentVariable("CUPPSCN", EnvironmentVariableTarget.User);
            textBoxACN.Text = System.Environment.GetEnvironmentVariable("CUPPSACN", EnvironmentVariableTarget.User);

            textBoxOPR.Text = System.Environment.GetEnvironmentVariable("CUPPSOPR", EnvironmentVariableTarget.User);
            textBoxUN.Text = System.Environment.GetEnvironmentVariable("CUPPSUN", EnvironmentVariableTarget.User);

            textBoxXSDU.Text = System.Environment.GetEnvironmentVariable("CUPPSXSDU", EnvironmentVariableTarget.User);

        }

        //load text goals
        private void loadGoals()
        {
            richTextBoxGoals.AppendText("1. Read Environment Variables \n");
            richTextBoxGoals.AppendText("   CUPPSPLT    = Platform IATA code \n");
            richTextBoxGoals.AppendText("   CUPPSPN     = Platform node to connect to\n");
            richTextBoxGoals.AppendText("   CUPPSPP     = Node port number\n");
            richTextBoxGoals.AppendText("   CUPPSCN     = Computername\n");
            richTextBoxGoals.AppendText("   CUPPSACN    = Alternate Computername depending on the Operator\n");
            richTextBoxGoals.AppendText("\n");
            richTextBoxGoals.AppendText("   CUPPSOPR    = Operator Code\n");
            richTextBoxGoals.AppendText("   CUPPSUN     = Username\n");
            richTextBoxGoals.AppendText("\n");
            richTextBoxGoals.AppendText("   CUPPSXSDU   = XSD Schema location (UNC)\n");
            richTextBoxGoals.AppendText("\n");
            richTextBoxGoals.AppendText("2. Create Network Socket \n");
            richTextBoxGoals.AppendText("\n");
            richTextBoxGoals.AppendText("3. Sending the initial XML message  : <interfaceLevelsAvailableRequest>  \n");
            richTextBoxGoals.AppendText("\n");
            richTextBoxGoals.AppendText("4. Displaying the platform response \n");
        }

        /*
         Sockets
         */

        //connection-disconnection
        private void buttonConnect_Click(object sender, EventArgs e)
        {

            int port = int.Parse(textBoxPort.Text);
            string host = textBoxHost.Text;

            IPAddress[] addresses = Dns.GetHostAddresses(host);

            //if client does not exist
            if (clientSocket == null)
            {
                clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                DataReceived = new Thread(new ThreadStart(ReadMsg));

                messageID = 0;
                generateXML();
            }


            //try to connect/disconnect
            try
            {
                if (!clientSocket.Connected)
                    connect(host, port);
                else
                {
                    //disconnect(host);

                    MessageBox.Show("Please send byeRequest to disconnect from the platform", "Airline Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (SocketException ex)
            {
                listViewEventsAdd(ex.Message);
            }
        }

        private void connect(String host, int port)
        {
            clientSocket.Connect(host, port);

            listViewEventsAdd("Connected to " + host);

            buttonConnect.Text = "1 - Network Socket Disconnection";

            //launch Thread and wait for message from server
            DataReceived.Start();
        }


        private void disconnect(String host)
        {
            listViewEventsAdd("Disconnected to " + host);
            buttonConnect.Text = "1 - Network Socket Connection";

            clientSocket.Close();
            DataReceived.Abort();

            clientSocket = null;
            DataReceived = null;
        }


        //load message
        private void buttonXML_Click(object sender, EventArgs e)
        {
            if (clientSocket != null)
            {
                messageID++;

                //replace XML vars
                String XML = richTextBoxXML.Text;
                XML = XML.Replace("{MESSAGEID}", messageID.ToString());
                XML = XML.Replace("{XSDVERSION}", XSDVersion);

                // send header msg
                XML = generateMsgHeader(XML) + XML;

                //send message
                SendMsg(XML);
            }
        }

        //write
        private void SendMsg(string message)
        {

            //encode string
            byte[] msg = System.Text.Encoding.UTF8.GetBytes(message);

            int DtSent = clientSocket.Send(msg);

            if (DtSent == 0)
            {
                richTextBoxLogAppendText('E', "No message sent");
                listViewEventsAdd("Error : No message sent");
            }
            else
            {
                richTextBoxLogAppendText('S', message);
                listViewEventsAdd("Sending " + message);
            }

        }

        private String generateMsgHeader(String XML) {
		    // send header msg
		    String version = "01";
		    String convention = "00";
            String size = XML.Length.ToString("X").ToUpper();


		    while (size.Length < 6)
			    size = "0" + size;

            Console.WriteLine(XML.Length);
            Console.WriteLine(size);



		    return version + convention + size;
	    }

        //read
        private void ReadMsg()
        {
            try
            {
                while (true)
                {
                    if (clientSocket.Connected)
                    {
                        //if client get data
                        if (clientSocket.Available > 0)
                        {

                            string messageReceived = null;
                            string rtfContent = null;

                            while (clientSocket.Available > 0)
                            {
                                try
                                {
                                    byte[] msg = new Byte[clientSocket.Available];

                                    //receive them

                                    clientSocket.Receive(msg, 0, clientSocket.Available, SocketFlags.None);

                                    messageReceived = System.Text.Encoding.UTF8.GetString(msg).Trim();

                                    //It concatenates the received data (4k max) in a variable

                                    rtfContent += messageReceived;

                                }

                                catch (SocketException E)
                                {
                                    //richTextBoxLogAppendText("CheckData read" + E.Message);
                                    this.Invoke(new richTextBoxLogAppend(richTextBoxLogAppendText), 'E', E.Message);

                                }
                            }

                            try
                            {
                                //Filling the richtextbox with the data received 
                                // when it was all approved

                                this.Invoke(new richTextBoxLogAppend(richTextBoxLogAppendText), 'R', rtfContent);

                            }

                            catch (Exception E)
                            {
                                MessageBox.Show(E.Message);
                            }

                        }

                    }

                    //We wait for 10 milliseconds, to avoid the microprocessor runaway

                    Thread.Sleep(10);
                }

            }

            catch
            {
                //This thread is likely to be arrested at any time 
                //catch the exception so as not to display a message to the user

                Thread.ResetAbort();
            }

        }

        /*
         
         */


        //function assigned to the delegate
        private void richTextBoxLogAppendText(char T, string str)
        {
            string symbol = (T == 'S') ? "-->" : "<--";


            richTextBoxLog.AppendText(T + " " + symbol + " " + DateTime.Now.ToString("dd/mm/yyyy HH:mm:ss.fff") + " :\n" + str + "\n\r");
            richTextBoxLog.ScrollToCaret();
        }

        //add Events in ListView
        private void listViewEventsAdd(string str)
        {
            ListViewItem lvItem = new ListViewItem(DateTime.Now.ToString("HH:mm:ss.fff"));
            lvItem.SubItems.Add(str);

            listViewEvents.Items.Add(lvItem);
        }

        //increase messageID in XML Message
        private void generateXML()
        {
            //create request message interfaces
            String XML = "<cupps messageID=\"{MESSAGEID}\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" messageName=\"interfaceLevelsAvailableRequest\">";
            XML += "<interfaceLevelsAvailableRequest hsXsdVersion=\"{XSDVERSION}\"/>";
            XML += "</cupps>";

            richTextBoxXML.Text = XML;
        }

        /*
         other events
         */

        //clear messages whend doubleclick
        private void richTextBoxLog_DoubleClick(object sender, EventArgs e)
        {
            richTextBoxLog.Clear();
        }

        //before closing, checks whether the socket is not connected
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (clientSocket != null && clientSocket.Connected)
            {
                //MessageBox.Show("You are connected !", "Airline Application");
                //e.Cancel = true;
                disconnect(textBoxHost.Text);
            }
            else
            {
                /*
                // Display a MsgBox asking the user to save changes or abort.
                if (MessageBox.Show("Are you sure you want to quit ?", "Airline Application", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    // Cancel the Closing event from closing the form.
                    e.Cancel = true;

                }
                */
            }
        }

    }
}
